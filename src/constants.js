export const speedValues = [
  {
    value: 500,
    label: 'Slow',
  },
  {
    value: 300,
    label: 'Normal',
  },
  {
    value: 100,
    label: 'Fast',
  },
];

export const defaultSettings = {
  speed: 300,
  rows: 30,
  columns: 30,
};

export const fieldSizeConfig = {
  step: 10,
  maxValue: 70,
};
