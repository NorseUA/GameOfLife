const getters = {
  fieldSize: ({ fieldSize }) => fieldSize,
  colony: ({ colony }) => colony,
  colonyIsAlive: ({ colonyIsAlive }) => colonyIsAlive,
  speedOfGame: ({ speedOfGame }) => speedOfGame,
};

export default getters;
